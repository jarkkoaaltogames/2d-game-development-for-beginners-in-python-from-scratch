# 2D Game Development for Beginners in Python from scratch

About this course
Beginner course Developing a 2D game purely using python pygame from scratch

### Python setup
install Python 3.x https://www.python.org/downloads/

install pygame library
```
pip install pygame
```

